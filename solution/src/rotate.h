#ifndef ASSIGNMENT_3_IMAGE_ROTATION_MASTER_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_MASTER_ROTATE_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
    uint8_t b, g, r;
} pixel;

typedef struct {
    uint64_t width, height;
    pixel *data;
} image;

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp(FILE *in, image *img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, image const *img);

image rotate(image *const source, int angle);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_MASTER_ROTATE_H
