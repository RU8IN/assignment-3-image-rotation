#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "rotate.h"

int main(int argc, char **argv) {
    int code = 0;
    if (argc != 4) {
        fprintf(stderr, "Expected  <source-image> <transformed-image> <angle> ");
        return 1;
    }

    FILE *input = fopen(argv[1], "rb");
    FILE *output = fopen(argv[2], "wb");
    int angle = atoi(argv[3]);
    if (angle < 0) angle += 360;

    if (output == NULL) {
        fprintf(stderr, "Cant open output file!");
        code = 1;
        goto clean;
    }

    image img = {0};
    image img_rotated = {0};

    if (from_bmp(input, &img) != READ_OK) {
        code = 1;
        goto clean;
    }

    img_rotated = rotate(&img, angle);

    if (to_bmp(output, &img_rotated) != WRITE_OK) {
        code = 1;
        goto clean;
    }

    clean:
    if (input != NULL) fclose(input);
    if (output != NULL) fclose(output);
    if (img.data != NULL) free(img.data);
    if (img_rotated.data != NULL) free(img_rotated.data);
    return code;
}
