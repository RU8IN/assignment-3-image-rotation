#include "rotate.h"
#include <memory.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} header;
#pragma pack(pop)

typedef struct {
    uint64_t x;
    uint64_t y;
} position;

uint64_t get_padding(uint64_t w) {
    uint64_t result = 4 - (w * 3) % 4;
    if (result == 4) return 0;
    return result;
}

uint64_t get_image_size(uint32_t width, uint32_t height) {
    return (width * 3 + get_padding(width)) * height;
}

pixel get_pixel(image const *img, uint64_t x, uint64_t y) {
    pixel p = {0};
    uint64_t offset = (x + y * img->width) * 3 + get_padding(img->width) * y;
    memcpy(&p, (uint8_t *) img->data + offset, 3);
    return p;
}

void set_pixel(image *img, pixel *p, uint64_t x, uint64_t y) {
    uint64_t offset = (x + y * img->width) * 3 + get_padding(img->width) * y;
    memcpy((uint8_t *) img->data + offset, p, 3);
}

enum read_status from_bmp(FILE *in, image *img) {
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Cant read bmp header from input!");
        return READ_ERROR;
    }

    if (header.bfType != 0x4d42) {
        fprintf(stderr, "Invalid signature!");
        return READ_INVALID_SIGNATURE;
    }

    if (header.bfReserved != 0) {
        fprintf(stderr, "Invalid header, reserved must be 0!");
        return READ_INVALID_HEADER;
    }

    if (header.biSize != 40) {
        fprintf(stderr, "Invalid header, biSize must be 40!");
        return READ_INVALID_HEADER;
    }

    if (header.biBitCount != 24) {
        fprintf(stderr, "Invalid header, bitCount must be 24!");
        return READ_INVALID_BITS;
    }

    img->height = header.biHeight,
    img->width = header.biWidth,
    img->data = malloc(header.biSizeImage);

    if (img->data == NULL) {
        fprintf(stderr, "Cant allocate memory!");
        return READ_ERROR;
    }

    if (fread(img->data, 1, header.biSizeImage, in) != header.biSizeImage) {
        fprintf(stderr, "Cant read img data!");
        return READ_ERROR;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, image const *img) {
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        fprintf(stderr, "Cant write bmp header!");
        return WRITE_ERROR;
    }
    if (fwrite(img->data, 1, header.biSizeImage, out) != header.biSizeImage) {
        fprintf(stderr, "Cant write image data!");
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

void swap(position *p) {
    uint64_t tmp = p->x;
    p->x = p->y;
    p->y = tmp;
}

position get_position(image const *img, uint64_t x, uint64_t y, int angle) {
    position p = {
            .x = x,
            .y = y
    };

    if (angle == 180) {
        p.y = img->height - p.y - 1;
        p.x = img->width - p.x - 1;
    }

    if (angle == 90) {
        swap(&p);
        p.x = img->width - p.x - 1;
    }

    if (angle == 270) {
        swap(&p);
        p.y = img->height - p.y - 1;
    }


    return p;
}

image rotate(image *const source, int angle) {
    uint64_t padding;
    image result = {0};

    if (angle % 180 == 0) {
        result.width = source->width;
        result.height = source->height;
    } else {
        header.biWidth = result.width = source->height;
        header.biHeight = result.height = source->width;
        padding = get_padding(result.width);
        header.biSizeImage = get_image_size(result.width, result.height);
    }
    result.data = malloc(header.biSizeImage);

    if (result.data == NULL) {
        return result;
    }

    for (int i = 0; i < result.height; i++) {
        for (int j = 0; j < result.width; j++) {
            position p = get_position(source, j, i, angle);
            pixel p1 = get_pixel(source, p.x, p.y);
            set_pixel(&result, &p1, j, i);
        }
    }

    return result;
}
